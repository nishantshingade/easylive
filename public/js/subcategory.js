$(document).on('click', '.startBtn', function () {
  var error = 0;data = [];
    var service_id = $(this).attr('data-id');
    var from = $('#from_date_'+service_id).val();
    var to = $('#to_date_'+service_id).val();
    var customer_id = $('#customer_id').val();
    
    if(from == ''){
      error =1;
      alert('Please Select Service Start Date');
      e.preventDefaut();
    }
    if(to == ''){
      error =1;
      alert('Please Select Service End Date');
      e.preventDefaut();
    }

    if(error != 1){

      swal({
          title: "Success!",
          type:"success",
          text: "You have successfully subscribed to this service. You can view your Active services on Homepage!",
          confirmButtonColor: "green",
          confirmButtonText: "ok",
          buttonsStyling: true,
          width: '350px',
          timer: 8000
      }).then(function (isconfirm) {
           if (isconfirm) {
                $.ajax({
                  headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
               url: base_url+'/service/start',
               method:'POST',
               data: {
                    data: { from: from, service_id: service_id, to:to, customer_id:customer_id }
                },
               success:function(response)
               {
                if(response){
                   window.location.href=base_url+'/mobile/dashboard';
                }else{
                  alert('Something Went Wrong !');
                }
               }
              });
            }else{
              e.preventDefault();
            }
      });
    }

});

$(document).on('click', '.getAns', function () {
    var key1 = $('#arrayAvalues').val().split(",");
    var returnObj = {};
    var data = '';
    $.each(key1, function(key,value) {
      var numOccr = $.grep(key1, function (elem) {
          return elem === value;
      }).length;
      returnObj[value] = numOccr;
    });
    $.each(returnObj, function(key,value) {
    data+="<tr><td>"+key+"</td><td>"+value+"</td>";
  });
    $('.occuranceTable').append(data);
});


$(document).on('click','.getAnsq3',function(){
     var key = $('#arrayAvalues').val();
     var data = '';var returnObj = {};
        $.ajaxSetup({
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        }); 
       jQuery.ajax({
          url: '/q3/ans',
          method: 'POST',
          data: {
             key: key
          },
          success: function(result){
          if(result == ''){
            data+="<tr><td>No Data Found</td></tr>";
          }else{
            $.each(result, function(key,value) {
                var digits = (""+value).split("");
                data+="<tr><td>"+(key+1)+"</td><td>"+digits+"</td>";
            });
          }     
            $('.occuranceTable').append(data);

          }
      });
});


