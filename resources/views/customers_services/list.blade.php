@extends('adminlte::page')
@section('content')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <div class="companydata">
        <table class="table table-bordered table-hover">
            <thead>
                <th>Sr.No</th>
                <th>Customer Name</th>
                <th>Service</th>
                <th>Category</th>
                <th>Sub Category</th>
                <th>Rate</th>
                <th>Commission</th>
                <th>total</th>
                <th>Start At</th>
                <th>End At</th>
                <th>Total Days</th>
                <th>Coupens</th>
                <th>Status</th>
                <th>Created At</th>
                <th>Action</th>
            </thead>
            <tbody>
                    @foreach ($list as $service)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $service->customers->fname }} {{ $service->customers->lname }}</td>
                        <td>{{ $service->services->name }}</td>
                        <td>{{ $service->category->name }}</td>
                        <td>{{ $service->subcatagory->name }}</td>
                        <td>{{ $service->rate }}</td>
                        <td>{{ $service->commission }}</td>
                        <td>{{ $service->total }}</td>
                        <td>{{ $service->start_date }}</td>
                        <td>{{ $service->end_date }}</td>
                        <td>{{ $service->no_of_days }}</td>
                        <td>{{ $service->milk_coupens }}</td>
                        <td>{{ $service->status }}</td>
                        <td>{{ $service->created_at }}</td>
                        <td>
                            <a href="{{ url('/customer/service/view', [$service->id]) }}" class="viewCategory"><i class="fa fa-eye text-green" title="View {{$service->name}}"></i></a>&nbsp;&nbsp;

                            <a href="{{ url('/customer/service/edit', [$service->id]) }}" class="editCategory"><i class="fa fa-pencil-square-o" title="Edit {{$service->name}}" aria-hidden="true"></i></a>&nbsp;&nbsp;

                            <a href="{{ url('/customer/service/delete', [$service->id]) }}" class="deleteCategory"><i class="fa fa-trash text-red" title="Delete {{$service->name}}"></i></a>
                        </td>
                    <tr>
                    @endforeach
            </tbody>
        </table>

    </div>


{{ $list->onEachSide(5)->links() }}

@endsection

@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@endsection
