@extends('adminlte::page')
@section('content')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <div class="companydata">
        <table class="table table-bordered table-hover">
            <thead>
                <th>Sr.No</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Society</th>
                <th>Wing</th>
                <th>Floor</th>
                <th>Flat</th>
                <th>Gender</th>
                <th>Status</th>
                <th>Crerated at</th>
                <th>Action</th>
            </thead>
            <tbody>
                    @foreach ($list as $customer)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $customer->fname }} {{ $customer->lname }}</td>
                        <td>{{ $customer->mobile }}</td>
                        <td>{{ $customer->email }}</td>
                        <td>{{ $customer->society->name }}</td>
                        <td>{{ $customer->wing }}</td>
                        <td>{{ $customer->floor }}</td>
                        <td>{{ $customer->flat }}</td>
                        <td>{{ $customer->gender }}</td>
                        <td>{{ $customer->status }}</td>
                        <td>{{ $customer->created_at }}</td>
                        <td>
                            <a href="{{ url('/customer/view', [$customer->id]) }}" class="viewCategory"><i class="fa fa-eye text-green" title="View {{$customer->name}}"></i></a>&nbsp;&nbsp;

                            <a href="{{ url('/customer/edit', [$customer->id]) }}" class="editCategory"><i class="fa fa-pencil-square-o" title="Edit {{$customer->name}}" aria-hidden="true"></i></a>&nbsp;&nbsp;

                            <a href="{{ url('/customer/delete', [$customer->id]) }}" class="deleteCategory"><i class="fa fa-trash text-red" title="Delete {{$customer->name}}"></i></a>
                        </td>
                    <tr>
                    @endforeach
            </tbody>
        </table>
        {{ $list->onEachSide(5)->links() }}
    </div>




@endsection

@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@endsection
