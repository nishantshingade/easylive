@extends('adminlte::page')
@section('content')
<div class="container">
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <!-- @if ($message = Session::get('errors'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                @foreach($errors->all() as $error)
                    <strong>{{ $error }}</strong><br>
                @endforeach
        </div>
    @endif -->

        <div class="card">
            <div class="card-header text-center bg-info">
            <b>ADD NEW Customer</b>
            </div>
            <div class="card-body">
                <form name="createCustomerForm" method="post" action="{{route('storeCustomers')}}" class="createCustomerForm" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="ismobile" value="0"/>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="serviceName">First Name</label><span style="color:red;">{{$errors->first('fname')}}</span>
                            <input type="text" class="form-control" id="fname" value="{{old('fname')}}" name="fname" aria-describedby="First Name" placeholder="Enter First Name">

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="serviceName">Last Name</label><span style="color:red;">{{$errors->first('lname')}}</span>
                            <input type="text" class="form-control" id="lname" value="{{old('lname')}}" name="lname" aria-describedby="Last Name" placeholder="Enter Last Name">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Mobile">Mobile No</label><span style="color:red;">{{$errors->first('mobile')}}</span>
                            <input type="number" class="form-control" id="mobile" value="{{old('mobile')}}" name="mobile" aria-describedby="mobile" placeholder="Enter Mobile Number">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Email">Email Address</label><span style="color:red;">{{$errors->first('email')}}</span>
                            <input type="text" name="email" class="form-control" value="{{old('email')}}" aria-describedby="email" placeholder="Enter Email Address" />
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="gender">Gender</label><span style="color:red;">{{$errors->first('gender')}}</span>
                            <select class="form-control" name="gender" id="gender"> 
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="ServiceLogo">Customer Pic</label><span style="color:red;">{{$errors->first('logo')}}</span>
                            <input type="file" name="logo" class="form-control" accept="image/*" />
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="subcategory">Society</label><span style="color:red;">{{$errors->first('society_id')}}</span>
                            <select class="form-control" name="society_id" id="society_id"> 
                                @foreach($societies as $society)
                                <option value="{{$society->id}}">{{$society->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="serviceName">Wing</label><span style="color:red;">{{$errors->first('wing')}}</span>
                            <input type="text" class="form-control" id="wing" value="{{old('wing')}}" name="wing" aria-describedby="wing" placeholder="Enter Wing Name">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="serviceName">Floor No</label><span style="color:red;">{{$errors->first('floor')}}</span>
                            <input type="text" class="form-control" id="floor" value="{{old('floor')}}" name="floor" aria-describedby="Floor No" placeholder="Enter Floor No">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="serviceName">Flat No</label><span style="color:red;">{{$errors->first('flat')}}</span>
                            <input type="text" class="form-control" id="flat" value="{{old('flat')}}" name="flat" aria-describedby="Flat No" placeholder="Enter Flat No">
                        </div>
                    </div>  
                    <div class="col-6">
                        <div class="form-group">
                            <label for="password">Password</label><span style="color:red;">{{$errors->first('password')}}</span>
                            <input type="password" class="form-control" id="password" value="{{old('password')}}" name="password" aria-describedby="password" placeholder="Enter Password">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="password">Confirm Password</label><span style="color:red;">{{$errors->first('password_confirmation')}}</span>
                            <input type="password" class="form-control" id="password_confirmation" value="{{old('password_confirmation')}}" name="password_confirmation" aria-describedby="password_confirmation" placeholder="Enter Password">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Company Contact">Created By</label>
                            <input type="text" class="form-control" id="created_by" name="created_by" data-id="{{ Auth::user()->id}}" value="{{ Auth::user()->id }}" aria-describedby="Created By" readonly>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div id="toggles">
                                <label for="Company Status">Status</label>
                                <input type="hidden" name="status" value="0">
                                <input type="checkbox" name="status" id="status" class="ios-toggle" checked/>
                                <label for="status" class="checkbox-label" data-off="Inactive" data-on="Active"></label>
                            </div>
                        </div>
                    </div>
                </div>
                    <input type="submit" name="submitForm" class="btn btn-success">
                </form>
            </div>
            <div class="card-footer text-center">
            <b>Live Easy</b>
            </div>
        </div>
    </div>
@endsection

@section('css')
<link href="{{ asset('css/employee.css') }}" rel="stylesheet">
@endsection