@extends('adminlte::page')
@section('content')
<div class="container">
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <!-- @if ($message = Session::get('errors'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                @foreach($errors->all() as $error)
                    <strong>{{ $error }}</strong><br>
                @endforeach
        </div>
    @endif -->

        <div class="card">
            <div class="card-header text-center bg-info">
            <b>ADD NEW Service</b>
            </div>
            <div class="card-body">
                <form name="createServiceForm" method="post" action="{{route('storeServices')}}" class="createServiceForm" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="serviceName">Service Name</label><span style="color:red;">{{$errors->first('name')}}</span>
                            <input type="text" class="form-control" id="name" value="{{old('name')}}" name="name" aria-describedby="Society Name" placeholder="Enter Service Name">

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Address">Service description</label><span style="color:red;">{{$errors->first('desc')}}</span>
                            <input type="text" class="form-control" id="desc" value="{{old('desc')}}" name="desc" aria-describedby="description" placeholder="Enter Service Description">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="ServiceLogo">Service Logo</label><span style="color:red;">{{$errors->first('logo')}}</span>
                            <input type="file" name="logo" class="form-control" accept="image/*" />
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="ServiceLogo">Service Rate</label><span style="color:red;">{{$errors->first('rate')}}</span>
                            <input type="text" name="rate" class="form-control" id="rate" value="{{old('rate')}}" aria-describedby="rate" placeholder="Enter Service Rate"/>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="ServiceLogo">Service Commission</label><span style="color:red;">{{$errors->first('commission')}}</span>
                            <input type="text" name="commission" class="form-control" id="commission" value="{{old('commission')}}" aria-describedby="commission" placeholder="Enter Service Commission"/>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="category_id">Category</label><span style="color:red;">{{$errors->first('category')}}</span>
                            <select class="form-control" name="category_id" id="category_id"> 
                                @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="subcatagories_id">Sub Category</label><span style="color:red;">{{$errors->first('subcategory')}}</span>
                            <select class="form-control" name="subcatagories_id" id="subcatagories_id"> 
                                @foreach($subcategories as $subcategory)
                                <option value="{{$subcategory->id}}">{{$subcategory->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Company Contact">Created By</label>
                            <input type="text" class="form-control" id="created_by" name="created_by" data-id="{{ Auth::user()->id}}" value="{{ Auth::user()->id }}" aria-describedby="Created By" readonly>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div id="toggles">
                                <label for="Company Status">Status</label>
                                <input type="hidden" name="status" value="0">
                                <input type="checkbox" name="status" id="status" class="ios-toggle" checked/>
                                <label for="status" class="checkbox-label" data-off="Inactive" data-on="Active"></label>
                            </div>
                        </div>
                    </div>
                </div>
                    <input type="submit" name="submitForm" class="btn btn-success">
                </form>
            </div>
            <div class="card-footer text-center">
            <b>Live Easy</b>
            </div>
        </div>
    </div>
@endsection

@section('css')
<link href="{{ asset('css/employee.css') }}" rel="stylesheet">
@endsection