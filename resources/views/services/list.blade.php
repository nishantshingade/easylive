@extends('adminlte::page')
@section('content')
<div class="container">
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <div class="companydata">
        <table class="table table-bordered table-hover">
            <thead>
                <th>Sr.No</th>
                <th>Service Name</th>
                <th>Description</th>
                <th>Category</th>
                <th>Sub Category</th>
                <th>Rate</th>
                <th>Commission</th>
                <th>logo</th>
                <th>Created At</th>
                <th>Status</th>
                <th>Action</th>
            </thead>
            <tbody>
                    @foreach ($list as $service)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $service->name }}</td>
                        <td>{{ $service->desc }}</td>
                        <td>{{ $service->category->name }}</td>
                        <td>{{ $service->subcatagory->name }}</td>
                        <td>{{ $service->rate }}</td>
                        <td>{{ $service->commission }}</td>
                        <td><img src="{{ asset('storage/images/services/'.$service->logo) }}" style="width: 100px;height: 100px;" alt="logo" title="" /></td>
                        <td>{{ $service->created_at }}</td>
                        <td>{{ $service->status }}</td>
                        <td>
                            <a href="{{ url('/service/view', [$service->id]) }}" class="viewCategory"><i class="fa fa-eye text-green" title="View {{$service->name}}"></i></a>&nbsp;&nbsp;

                            <a href="{{ url('/service/edit', [$service->id]) }}" class="editCategory"><i class="fa fa-pencil-square-o" title="Edit {{$service->name}}" aria-hidden="true"></i></a>&nbsp;&nbsp;

                            <a href="{{ url('/service/delete', [$service->id]) }}" class="deleteCategory"><i class="fa fa-trash text-red" title="Delete {{$service->name}}"></i></a>
                        </td>
                    <tr>
                    @endforeach
            </tbody>
        </table>

    </div>


{{ $list->onEachSide(5)->links() }}

</div>
@endsection

@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@endsection
