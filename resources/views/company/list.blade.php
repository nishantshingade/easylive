@extends('adminlte::page')
@section('content')
<div class="container">
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <div class="companydata">
        <table class="table table-bordered table-hover">
            <thead>
                <th>Sr.No</th>
                <th>Company Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Contact</th>
                <th>Year</th>
                <th>Status</th>
            </thead>
            <tbody>
                    @foreach ($list as $company)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $company->name }}</td>
                        <td>{{ $company->email }}</td>
                        <td>{{ $company->address }}</td>
                        <td>{{ $company->contact }}</td>
                        <td>{{ $company->establishment }}</td>
                        <td>{{ $company->status }}</td>
                    <tr>
                    @endforeach
            </tbody>
        </table>

    </div>


{{ $list->onEachSide(5)->links() }}

</div>
@endsection

@section('css')
<link href="{{ asset('css/employee.css') }}" rel="stylesheet">
@endsection