@extends('adminlte::page')
@section('content')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <!-- @if ($message = Session::get('errors'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                @foreach($errors->all() as $error)
                    <strong>{{ $error }}</strong><br>
                @endforeach
        </div>
    @endif -->
   <form name="invoiceForm" method="post" action="{{route('invoiceShow')}}">
   @csrf
      <div class="form-row align-items-center">
        <div class="col-auto"><span style="color:red;">{{$errors->first('society')}}</span>
          <select class="custom-select mr-sm-2" name="society" id="inlineFormCustomSelect">
            <option selected>Select Society</option>
            @foreach($societies as $society)
                <option value="{{$society->id}}">{{$society->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-auto">
          <label class="sr-only" for="inlineFormInput">Wing</label><span style="color:red;">{{$errors->first('wing')}}</span>
          <input type="text" name="wing" value="{{old('wing')}}" class="form-control mb-2" id="inlineFormInput" placeholder="Society Wing">
        </div>
        <div class="col-auto">
          <label class="sr-only" for="inlineFormInput">Flat No</label><span style="color:red;">{{$errors->first('flat')}}</span>
          <input type="text" name="flat" value="{{old('flat')}}" class="form-control mb-2" id="inlineFormInputGroup" placeholder="Flat No">
        </div>
        <div class="col-auto">
          <label class="sr-only" for="inlineFormInputGroup">Mobile Number</label><span style="color:red;">{{$errors->first('mobile')}}</span>
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></div>
            </div>
            <input type="number" name="mobile" value="{{old('mobile')}}" class="form-control" id="inlineFormInputGroup" placeholder="Mobile Number">
          </div>
        </div>
        <div class="col-auto">
          <label class="sr-only" for="inlineFormInput">Select Month</label><span style="color:red;">{{$errors->first('month')}}</span>
          <input type="month" class="form-control" id="month" name="month">
        </div>


        <div class="col-auto">
          <button type="submit" class="btn btn-primary mb-2">Submit</button>
        </div>
        <div class="col-auto">
          <a href="{{route('invoice')}}" class="btn btn-danger mb-2">Reset</a>
        </div>
      </div>
</form>

<div class="companydata">
        <table class="table table-bordered table-hover">
            <thead>
                <th>Sr.No</th>
                <th>Customer Name</th>
                <th>Mobile</th>
                <th>Wing</th>
                <th>Floor</th>
                <th>Flat No</th>
                <th>Services</th>
                <th>Action</th>
            </thead>
            <tbody>
                    @foreach ($list as $customer)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $customer->fname }} {{ $customer->lname }}</td>
                        <td>{{ $customer->mobile }}</td>
                        <td>{{ ucfirst($customer->wing) }}</td>
                        <td>{{ $customer->floor }}</td>
                        <td>{{ $customer->flat }}</td>
                        <td>
                        @if ($customer->serviceList != "")
                          @foreach(explode(',', $customer->serviceList) as $service) 
                            <a href="#" class="btn btn-success btn-sm mb-1">{{ $service }}</a>
                          @endforeach
                        @endif
                        </td>
                        <td>
                        <a href="{{ url('/customers/invoice/print', [$customer->customers_id,$month]) }}" class="viewCategory"><i class="fa fa-eye text-green" title="View Invoice"></i></a>
                        </td>
                    <tr>
                    @endforeach
            </tbody>
        </table>
        @if(!empty($list))
        {{ $list->onEachSide(5)->links() }}
        @endif
    </div>
        
@endsection

@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@endsection
