@extends('adminlte::page')
@section('content')
<!DOCTYPE html>
<html>
      <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-4 float-right">
        </div>
      </div>

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Invoice</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Invoice</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content" >
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3" style="border: 1px solid black;">
              <!-- title row -->
              <div class="row p-2">
                
                <div class="col-9 text-center">
                   <h2 class="pl-5"> Invoice</h2>
                </div>
                <div class="col-3 text-center">
                    <img src="{{ asset('storage/images/liveeasy/liveeasy-logo1.png') }}" style="width: 56%;" id="liveeasyLogo" alt="logo" title="" />
                </div>               
                <!-- /.col -->
              </div>
              <hr>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col" style="border-right: 1px solid black;">
                  <p class="mb-2"><b>FROM :</b></p>
                  <address>
                    <b>{{$from['name']}}</b><br>
                    Address : {{$from['address']}}<br>
                    Email : {{$from['email']}}<br>
                    Mobile :  {{$from['mobile']}}
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col" style="border-right: 1px solid black;">
                  <p class="mb-2"><b>TO :</b></p>
                  <address>
                    <b>{{$customer->fname}} {{$customer->lname}}</b><br>
                    Address : Wing -{{$customer->wing}}, Floor -{{$customer->floor}}, Flat No -{{$customer->flat}}<br>
                    {{$list[0]->societyName}},{{$list[0]->societyAddr}}<br>
                    Phone: {{$customer->mobile}}<br>
                    Email: {{$customer->email}}<br>
                  </address>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>Sr.No</th>
                      <th>Service Name</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>No of Days</th>
                      <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $service)
                    <tr>
                    <td>{{$loop->iteration}}</td>
                      <td>{{$service->serviceName}}</td>
                      <td>{{$service->start_date}}</td>
                      <td>{{$service->end_date}}</td>
                      <td>{{$service->no_of_days}}</td>
                      <td>{{$service->total}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <hr>
              <br>
              <!-- /.row -->
              <div class="row">
                <!-- accepted payments column -->
                <div class="col-3">
                <h4 class="lead"></h4>
                 

                  <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                    
                  </p>
                </div>
                <!-- /.col -->
                <div class="col-9">
                 <!--  <p class="lead"><b>Amount Paid</b></p> -->
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th>Total :</th>
                        <td><i class="fa fa-inr" aria-hidden="true"></i><b>{{$total}}</b></td>
                      </tr>
                      <tr>
                        <th>Amount Chargable(in Words):</th>
                        <td><i class="fa fa-inr" aria-hidden="true"></i><b>{{ucfirst($inWords)}}</b></td>
                      </tr>
                      
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row mt-5">
                <div class="col-6" style="float:left;margin-top:170px">
                  <b>E. & O.E.</b>
                </div>
                <div class="col-6 border border-secondary" style="height:200px;border-radius:20px;">
                  
                  <div clas="izyskStampUpper"style="position: relative;margin-left: 333px;margin-top: 3px;" >
                    <p style="float: right;position: absolute;">For Live Easy</p>
                     <p class="izyskStamp" style="float: right;margin-top:135px;position: relative;">Authorised Signatory</p>
                  </div>
                  
                </div>
              </div>

            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
    
        
@stop

@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <style>
      hr{
        border-top: 1px solid black;
      }
    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/subscription.js')}}"></script>

@stop