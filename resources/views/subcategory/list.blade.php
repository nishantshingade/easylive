@extends('adminlte::page')
@section('content')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <div class="companydata">
        <table class="table table-bordered table-hover">
            <thead>
                <th>Sr.No</th>
                <th>Sub Category Name</th>
                <th>Description</th>
                <th>Category Name</th>
                <th>logo</th>
                <th>Created At</th>
                <th>Status</th>
                <th>Action</th>
            </thead>
            <tbody>
                    @foreach ($list as $category)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->desc }}</td>
                        <td>{{ $category->category->name }}</td>
                        <td><img src="{{ asset('storage/images/subcategory/'.$category->logo) }}" style="width: 100px;height: 100px;" alt="logo" title="" /></td>
                        <td>{{ $category->created_at }}</td>
                        <td>{{ $category->status }}</td>
                        <td>
                            <a href="{{ url('/category/view', [$category->id]) }}" class="viewCategory"><i class="fa fa-eye text-green" title="View {{$category->name}}"></i></a>&nbsp;&nbsp;

                            <a href="{{ url('/category/edit', [$category->id]) }}" class="editCategory"><i class="fa fa-pencil-square-o" title="Edit {{$category->name}}" aria-hidden="true"></i></a>&nbsp;&nbsp;

                            <a href="{{ url('/category/delete', [$category->id]) }}" class="deleteCategory"><i class="fa fa-trash text-red" title="Delete {{$category->name}}"></i></a>
                        </td>
                    <tr>
                    @endforeach
            </tbody>
        </table>

    </div>


{{ $list->onEachSide(5)->links() }}
@endsection

@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@endsection
