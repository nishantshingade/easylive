@extends('adminlte::page')
@section('content')
<div class="container">
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <div class="companydata">
        <table class="table table-bordered table-hover">
            <thead>
                <th>Sr.No</th>
                <th>Society Name</th>
                <th>Chairman</th>
                <th>Contact</th>
                <th>Address</th>
                <th>Year</th>
                <th>Status</th>
                <th>Action</th>
            </thead>
            <tbody>
                    @foreach ($list as $society)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $society->name }}</td>
                        <td>{{ $society->chairman_name }}</td>
                        <td>{{ $society->contact }}</td>
                        <td>{{ $society->address }}</td>
                        <td>{{ $society->establishment }}</td>
                        <td>{{ $society->status }}</td>
                        <td>
                            <a href="{{ url('/society/view', [$society->id]) }}" class="viewSociety"><i class="fa fa-eye text-green" title="View {{$society->name}}"></i></a>&nbsp;&nbsp;

                            <a href="{{ url('/society/edit', [$society->id]) }}" class="editSociety"><i class="fa fa-pencil-square-o" title="Edit {{$society->name}}" aria-hidden="true"></i></a>&nbsp;&nbsp;

                            <a href="{{ url('/society/delete', [$society->id]) }}" class="deleteSociety"><i class="fa fa-trash text-red" title="Delete {{$society->name}}"></i></a>
                        </td>
                    <tr>
                    @endforeach
            </tbody>
        </table>

    </div>


{{ $list->onEachSide(5)->links() }}

</div>
@endsection

@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@endsection
