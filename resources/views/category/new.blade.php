@extends('adminlte::page')
@section('content')
<div class="container">
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <!-- @if ($message = Session::get('errors'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                @foreach($errors->all() as $error)
                    <strong>{{ $error }}</strong><br>
                @endforeach
        </div>
    @endif -->

        <div class="card">
            <div class="card-header text-center bg-info">
            <b>ADD NEW Category</b>
            </div>
            <div class="card-body">
                <form name="createCategoryForm" method="post" action="{{route('storeCategory')}}" class="createCategoryForm" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="categoryName">Category Name</label><span style="color:red;">{{$errors->first('name')}}</span>
                            <input type="text" class="form-control" id="name" value="{{old('name')}}" name="name" aria-describedby="Society Name" placeholder="Enter Company Name">

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Address">Category description</label><span style="color:red;">{{$errors->first('desc')}}</span>
                            <input type="text" class="form-control" id="desc" value="{{old('desc')}}" name="desc" aria-describedby="description" placeholder="Enter Category Description">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Establishment Date">Category Logo</label><span style="color:red;">{{$errors->first('logo')}}</span>
                            <input type="file" name="logo" class="form-control" accept="image/*" />
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div id="toggles">
                                <label for="Company Status">Status</label>
                                <input type="hidden" name="status" value="0">
                                <input type="checkbox" name="status" id="status" class="ios-toggle" checked/>
                                <label for="status" class="checkbox-label" data-off="Inactive" data-on="Active"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Company Contact">Created By</label>
                            <input type="text" class="form-control" id="created_by" name="created_by" data-id="{{ Auth::user()->id}}" value="{{ Auth::user()->id }}" aria-describedby="Created By" readonly>
                        </div>
                    </div>
                </div>
                    <input type="submit" name="submitForm" class="btn btn-success">
                </form>
            </div>
            <div class="card-footer text-center">
            @nishant
            </div>
        </div>
    </div>
@endsection

@section('css')
<link href="{{ asset('css/employee.css') }}" rel="stylesheet">
@endsection