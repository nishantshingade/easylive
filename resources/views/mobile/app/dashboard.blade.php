@extends('layouts.mobileapp')
@section('content')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
  <div class="container">
      <div class="row text-center">
        @foreach($categories as $category)
            <div class="col-md-2 d-flex justify-content-center">
                <div class="card shadow bg-white rounded" style="width: 12rem;margin-bottom: 10px;">
                  <div class="card-block text-center">
                    <a href="{{ url('mobile/category/'.$category->id) }}">
                        <img class="z-depth-2 mb-2" style="width: 100px; height: 100px; " alt="50x50" src="{{ asset('storage/images/category/'. $category->logo ) }}"
                          data-holder-rendered="true">
                        <h5 class="card-title">{{ $category->name }}</h5>
                    </a>    
                  </div>
                </div>
            </div>
        @endforeach
      </div>
      <hr>
      <div class="row text-center my-4">
        <div class="col-md-10 d-flex justify-content-center">
        <h3>My Active Services</h3>
        </div>
      </div>

      <div class="row">
      @foreach($myServices as $service)
        <div class="col-md-6 mb-4">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-4 border-right">
                    <img class="z-depth-2 my-2" style="width: 120px; height: 120px; " alt="50x50" src="{{ asset('storage/images/services/'. $service->services->logo ) }}"
                  data-holder-rendered="true">
                  </div>
                  <div class="col-md-8 text-center">
                    <div class="row text-center">
                        <div class="col-md-12 mb-3 border-bottom">
                          <b>Service Name :</b> {{ $service->services->name }}
                        </div>
                        <div class="col-md-6 mb-3 border-bottom">
                          <b>Start :</b> {{ $service->start_date }}
                        </div>
                        <div class="col-md-6 mb-3 border-bottom">
                          <b>End :</b> {{ $service->end_date }}
                        </div>
                        <div class="col-md-12">
                        @if($service->subcatagories_id == 16)
                        <div class="row text-center">
                          <div class="col-md-8">
                              <b>Coupons Left:</b> <span style="font-size: 30px;"><b>{{ $service->milk_coupens }}</b></span>
                          </div>
                            <div class="col-md-4">
                               <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModalCenter" >Stop</button>
                            </div>
                          </div>
                        @endif
                        </div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
        </div>
      @endforeach 
      </div>

      <!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Stop Service</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form name="serviceStopForm" method="post" action="{{route('stopCustomerServices')}}">
              <div class="form-group">
                <label for="stopfrom">Stop From</label>
                <input type="date" name="stopfrom" class="form-control" min="{{date('Y-m-d')}}" max="{{\Carbon\Carbon::now()->endOfMonth()->toDateString()}}"/>
              </div>
              <div class="form-group">
                <label for="stoptill">Stop Till</label>
               <input type="date" name="stoptill"  class="form-control" min="{{date('Y-m-d')}}" max="{{\Carbon\Carbon::now()->endOfMonth()->toDateString()}}"/>
              </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger">Stop !</button>
        </form>
      </div>
    </div>
  </div>
</div>

  </div>  
@endsection
