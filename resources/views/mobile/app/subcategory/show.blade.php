@extends('layouts.mobileapp')
@section('content')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
    <div class="row text-center">
      @foreach($services as $service)
        <!--  https://codepen.io/brunopereiradonascimento/pen/POyBgJ -->
        <div class="col-md-3 col-sm-6">
             <div class="card-container manual-flip">
                <div class="card shadow p-3 mb-5 bg-white rounded">
                    <div class="front">
                        <div class="user">
                            <img class="" src="{{ asset('storage/images/services/'.$service->logo ) }}"/>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h3 class="name">{{ $service->name}}</h3>
                            </div>
                            <div class="footer">
                                <button class="btn btn-simple" onclick="rotateCard(this)">
                                    <i class="fa fa-mail-forward"></i> Subscribe
                                </button>
                            </div>
                        </div>
                    </div> <!-- end front panel -->
                    <div class="back">
                        <div class="header">
                            <h5 class="motto">{{ $service->name}}</h5>
                        </div>
                        <div class="content">
                            <div class="main">
                                <div class="form-group">
                                  <input type="hidden" name="customer_id" id="customer_id" value="{{session('user')['id']}}">
                                  <label for="exampleFormControlInput1">From Date</label>
                                  <input type="date" class="form-control" id="from_date_{{ $service->id}}">
                                </div>
                                <div class="form-group">
                                  <label for="exampleFormControlInput1">To Date</label>
                                  <input type="date" class="form-control" id="to_date_{{ $service->id}}">
                                </div>
                                <div class="form-group">
                                  <button type="button" id="{{ $service->id}}" data-id="{{ $service->id}}" class="btn btn-outline-primary startBtn">Start Service</button>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <button class="btn btn-simple" rel="tooltip" title="Flip Card" onclick="rotateCard(this)">
                                <i class="fa fa-reply"></i> Back
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      @endforeach
      </div>
      <style>
        .btn:hover,
.btn:focus,
.btn:active{
    outline: 0 !important;
}
/* entire container, keeps perspective */
.card-container {
    -webkit-perspective: 800px;
   -moz-perspective: 800px;
     -o-perspective: 800px;
        perspective: 800px;
        margin-bottom: 30px;
}
/* flip the pane when hovered */
.card-container:not(.manual-flip):hover .card,
.card-container.hover.manual-flip .card{
  -webkit-transform: rotateY( 180deg );
-moz-transform: rotateY( 180deg );
 -o-transform: rotateY( 180deg );
    transform: rotateY( 180deg );
}


.card-container.static:hover .card,
.card-container.static.hover .card {
  -webkit-transform: none;
-moz-transform: none;
 -o-transform: none;
    transform: none;
}
/* flip speed goes here */
.card {
   -webkit-transition: -webkit-transform .5s;
   -moz-transition: -moz-transform .5s;
     -o-transition: -o-transform .5s;
        transition: transform .5s;
-webkit-transform-style: preserve-3d;
   -moz-transform-style: preserve-3d;
     -o-transform-style: preserve-3d;
        transform-style: preserve-3d;
  position: relative;
}

/* hide back of pane during swap */
.front, .back {
  -webkit-backface-visibility: hidden;
   -moz-backface-visibility: hidden;
     -o-backface-visibility: hidden;
        backface-visibility: hidden;
  position: absolute;
  top: 0;
  left: 0;
  background-color: #FFF;
    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.14);
}

/* front pane, placed above back */
.front {
  z-index: 2;
}

/* back, initially hidden pane */
.back {
    -webkit-transform: rotateY( 180deg );
   -moz-transform: rotateY( 180deg );
     -o-transform: rotateY( 180deg );
        transform: rotateY( 180deg );
        z-index: 3;
}

.back .btn-simple{
    position: absolute;
    left: 0;
    bottom: 4px;
}
/*        Style       */


.card{
    background: none repeat scroll 0 0 #FFFFFF;
    border-radius: 4px;
    color: #444444;
}
.card-container, .front, .back {
  width: 100%;
  height: 335px;
  border-radius: 4px;
}
.card .cover{
    height: 105px;
    overflow: hidden;
    border-radius: 4px 4px 0 0;
}
.card .cover img{
    width: 100%;
}
.card .user{
    display: block;
    height: 160px;
    margin: 15px auto 0;
    overflow: hidden;
    width: 120px;
}
.card .user img{
    background: none repeat scroll 0 0 #FFFFFF;
    border: 4px solid #FFFFFF;
    width: 100%;
}

.card .content{
    background-color: rgba(0, 0, 0, 0);
    box-shadow: none;
    padding: 10px 20px 20px;
}
.card .content .main {
    min-height: 50px;
}
.card .back .content .main {
    height: 215px;
}
.card .name {
    font-size: 22px;
    line-height: 28px;
    margin: 10px 0 0;
    text-align: center;
    text-transform: capitalize;
}
.card h5{
    margin: 5px 0;
    font-weight: 400;
    line-height: 20px;
}
.card .profession{
    color: #999999;
    text-align: center;
    margin-bottom: 20px;
}
.card .footer {
    border-top: 1px solid #EEEEEE;
    color: #999999;
    margin: 30px 0 0;
    padding: 10px 0 0;
    text-align: center;
}
.card .footer .social-links{
    font-size: 18px;
}
.card .footer .social-links a{
    margin: 0 7px;
}
.card .footer .btn-simple{
    margin-top: -6px;
}
.card .header {
    padding: 15px 20px;
    height: 50px;
}
.card .motto{
    border-bottom: 1px solid #EEEEEE;
    color: #999999;
    font-size: 14px;
    font-weight: 400;
    padding-bottom: 10px;
    text-align: center;
}
/*      Just for presentation        */

.title{
    color: #506A85;
    text-align: center;
    font-weight: 300;
    font-size: 44px;
    margin-bottom: 90px;
    line-height: 90%;
}
.title small{
    font-size: 17px;
    color: #999;
    text-transform: uppercase;
    margin: 0;
}
.space-30{
  height: 30px;
  display: block;
}
.space-50{
    height: 50px;
    display: block;
}
.space-200{
    height: 200px;
    display: block;
}
.white-board{
    background-color: #FFFFFF;
    min-height: 200px;
    padding: 60px 60px 20px;
}
.ct-heart{
    color: #F74933;
}

 pre.prettyprint{
    background-color: #ffffff;
    border: 1px solid #999;
    margin-top: 20px;
    padding: 20px;
    text-align: left;
}
.atv, .str{
    color: #05AE0E;
}
.tag, .pln, .kwd{
     color: #3472F7;
}
.atn{
  color: #2C93FF;
}
.pln{
   color: #333;
}
.com{
    color: #999;
}

.btn-simple{
    opacity: .8;
    color: #666666;
    background-color: transparent;
}

.btn-simple:hover,
.btn-simple:focus{
    background-color: transparent;
    box-shadow: none;
    opacity: 1;
}
.btn-simple i{
    font-size: 16px;
}

.navbar-brand-logo{
    padding: 0;
}
.navbar-brand-logo .logo{
    border: 1px solid #333333;
    border-radius: 50%;
    float: left;
    overflow: hidden;
    width: 60px;
}
.navbar .navbar-brand-logo .brand{
    color: #FFFFFF;
    float: left;
    font-size: 18px;
    font-weight: 400;
    line-height: 20px;
    margin-left: 10px;
    margin-top: 10px;
    width: 60px;
}
.navbar-default .navbar-brand-logo .brand{
    color: #555;
}


/*       Fix bug for IE      */

@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
    .front, .back{
        -ms-backface-visibility: visible;
        backface-visibility: visible;
    }

    .back {
        visibility: hidden;
        -ms-transition: all 0.2s cubic-bezier(.92,.01,.83,.67);
    }
    .front{
        z-index: 4;
    }
    .card-container:not(.manual-flip):hover .back,
    .card-container.manual-flip.hover .back{
        z-index: 5;
        visibility: visible;
    }
}

</style>
<script>
   var base_url = '{{ URL::to("/") }}';
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.all.min.js"></script>
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'>
<script type="text/javascript">
  $().ready(function(){
        $('[rel="tooltip"]').tooltip();

    });

    function rotateCard(btn){
        var $card = $(btn).closest('.card-container');
        console.log($card);
        if($card.hasClass('hover')){
            $card.removeClass('hover');
        } else {
            $card.addClass('hover');
        }
    }
</script>
@endsection
