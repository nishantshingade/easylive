@extends('layouts.mobileapp')
@section('content')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
  <div class="container">
      <div class="row text-center">
        @foreach($data as $category)
        @foreach($category->subcategories as $subcategory)
            <div class="col-md-2 d-flex justify-content-center">
                <div class="card" style="width: 12rem;margin-bottom: 10px;">
                  <div class="card-block text-center">
                    <a href="{{ url('mobile/subcategory/'.$subcategory->id) }}">
                        <img class="z-depth-2 mb-2" style="width: 100px; height: 100px; " alt="50x50" src="{{ asset('storage/images/subcategory/'.$subcategory['logo'] ) }}"
                          data-holder-rendered="true">
                        <h5 class="card-title">{{ $subcategory['name'] }}</h5>
                    </a>    
                  </div>
                </div>
            </div>
        @endforeach
        @endforeach
      </div>
  </div>  
@endsection