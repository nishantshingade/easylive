@extends('adminlte::page')
@section('content')
<div class="container">
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <div class="companydata">
        <table class="table table-bordered table-hover">
            <thead>
                <th>Sr.No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Contact</th>
                <th>DOB</th>
                <th>Company</th>
                <th>Status</th>
            </thead>
            <tbody>
                    @foreach ($list as $employee)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $employee->name }}</td>
                        <td>{{ $employee->email }}</td>
                        <td>{{ $employee->address }}</td>
                        <td>{{ $employee->contact }}</td>
                        <td>{{ $employee->dob }}</td>
                        <td>{{ $employee->company->name}}</td>
                        <td>{{ $employee->status }}</td>
                    <tr>
                    @endforeach
            </tbody>
        </table>

    </div>


{{ $list->onEachSide(5)->links() }}

</div>
@endsection

@section('css')
<link href="{{ asset('css/employee.css') }}" rel="stylesheet">
@endsection