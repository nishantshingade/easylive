@extends('adminlte::page')
@section('content')
<div class="container">
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
        <div class="card">
            <div class="card-header text-center bg-info">
            <b>ADD NEW Employee</b>
            </div>
            <div class="card-body">
                <form name="createEmployeeForm" method="post" action="{{route('storeEmployee')}}" class="createEmployeeForm" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="employeeName">Employee Name</label><span style="color:red;">{{$errors->first('name')}}</span>
                            <input type="text" class="form-control" id="name" value="{{old('name')}}" name="name" aria-describedby="Employee Name" placeholder="Enter Employee Name">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="companyEmail"> Email</label><span style="color:red;">{{$errors->first('email')}}</span>
                            <input type="text" class="form-control" id="email" value="{{old('email')}}" name="email" aria-describedby="Emloyee Email" placeholder="Enter Employee Email">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Address">Address</label><span style="color:red;">{{$errors->first('address')}}</span>
                            <input type="text" class="form-control" id="address" value="{{old('address')}}" name="address" aria-describedby="Employee Address" placeholder="Enter Employee Address">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Company Contact">contact No</label><span style="color:red;">{{$errors->first('contact')}}</span>
                            <input type="text" class="form-control" id="contact" value="{{old('contact')}}" name="contact" aria-describedby="Employee Contact" placeholder="Enter Employee Details ">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Establishment Date">Date Of Birth</label><span style="color:red;">{{$errors->first('dob')}}</span>
                            <input type="date" class="form-control" id="dob" value="{{old('dob')}}" name="dob" aria-describedby="Company Contact">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Company">Company</label><span style="color:red;">{{$errors->first('company')}}</span>
                            <select class="form-control" name="company_id" id="company_id"> 
                                @foreach($companies as $company)
                                <option value="{{$company->id}}">{{$company->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <div id="toggles">
                                <label for="Company Status">Status</label>
                                <input type="checkbox" name="status" id="status" class="ios-toggle" checked/>
                                <label for="status" class="checkbox-label" data-off="Inactive" data-on="Active"></label>
                            </div>
                        </div>
                    </div>
                </div>
                    <input type="submit" name="submitForm" class="btn btn-success">
                </form>
            </div>
            <div class="card-footer text-center">
            @nishant
            </div>
        </div>
    </div>
@endsection

@section('css')
<link href="{{ asset('css/employee.css') }}" rel="stylesheet">
@endsection