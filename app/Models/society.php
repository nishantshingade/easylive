<?php

namespace App\Models;
use fileHandler;
use App\Models\customers;
use App\Models\customer_services;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class society extends Model
{
    use HasFactory;
    public $table = "society";
    protected $guarded=['submitForm'];

    //this is mutator
	public function setStatusAttribute($value){
		$this->attributes['status'] =  isset($value) ? 1 : 0;
	}

	public function scopeActive($query){
		return $query->where('status','=',1)->orderBy('created_at', 'desc')->get();
	}

	//this is model scoping
	public function scopeShow($query){
		return $query->orderBy('created_at', 'desc')->paginate(10);
	}

	public function setLogoAttribute($value){
		$fileName = fileHandler::societyImageStore($value);
		$this->attributes['logo'] = $fileName;
	}

	public function customers(){
		return $this->hasMany(customers::class);
	}

	public function customer_services(){
        return $this->hasManyThrough(customer_services::class, customers::class);
    }

	//this is accessor
	public function getStatusAttribute($attribute){
		return [
		2 => 'Inactive',
		1=>'Active',
		3 => 'Draft',
		][$attribute];
	}

}
