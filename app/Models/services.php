<?php

namespace App\Models;
use fileHandler;
use App\Models\category;
use App\Models\subcatagory;
use App\Models\customers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\customer_services;

class services extends Model
{
    use HasFactory;
    public $table = "services";
    protected $guarded=['submitForm'];
    protected $casts = ['status' => 'boolean'];

    public function setStatusAttribute($value){
		if($value == 'on'){
			$this->attributes['status'] = 1;
		}
	}

	public function scopeShow($query){
		return $query->orderBy('created_at', 'desc')->paginate(10);
	}

	public function setLogoAttribute($value){
		$fileName = fileHandler::serviceImageStore($value);
		$this->attributes['logo'] = $fileName;
	}

	public function category(){
		return $this->belongsTo(category::class,'category_id');
	}

	public function subcatagory(){
		return $this->belongsTo(subcatagory::class,'subcatagories_id');
	}

	public function customers(){
		return $this->hasMany(customers::class,'customers_id');
	}

	public function customer_services(){
		return $this->belongsTo(customer_services::class);
	}

	//this is accessor
	public function getStatusAttribute($attribute){
		return [
		null => 'Inactive',
		0 => 'Inactive',
		1=>'Active',
		][$attribute];
	}
}
