<?php

namespace App\Models;
use App\Models\company;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    use HasFactory;
    protected $guarded=['submitForm'];

    public function setStatusAttribute($value){
		$this->attributes['status'] =  isset($value) ? 1 : 0;
	}

	public function scopeShow($query){
		return $query->orderBy('created_at', 'desc')->paginate(10);
	}

	public function getStatusAttribute($attribute){
		return [
		2 => 'Inactive',
		1=>'Active',
		3 => 'Draft',
		][$attribute];
	}

	public function company(){
		return $this->belongsTo(company::class);
	}
}
