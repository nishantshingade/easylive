<?php

namespace App\Models;
use App\Models\customers;
use App\Models\services;
use App\Models\category;
use App\Models\subcatagory;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class customer_services extends Model
{
    use HasFactory;
    public $table = "customer_services";
    protected $guarded=['submitForm'];
    protected $casts = ['status' => 'boolean'];

    public function myServices($query,$id){
    	return $query->where('customers_id', '=', $id)
		->select('child_cat_id','child_cat_name')
    	->get();
    }

    public function customers(){
		return $this->belongsTo(customers::class);
	}

	public function services(){
		return $this->belongsTo(services::class);
	}

	public function category(){
		return $this->belongsTo(category::class);
	}

	public function subcatagory(){
		return $this->belongsTo(subcatagory::class,'subcatagories_id');
	}

	/*public function society(){
        return $this->hasOneThrough('App\customer_services', 'App\services');
    }*/

	public function getStatusAttribute($attribute){
		return [
		null => 'Inactive',
		0 => 'Inactive',
		1=>'Active',
		][$attribute];
	}


}
