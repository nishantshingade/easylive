<?php

namespace App\Models;
use Hash;
use App\Models\society;
use fileHandler;
use App\Models\services;
use App\Models\customer_services;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class customers extends Model
{
    use HasFactory;
    public $table = "customers";
    protected $guarded=['submitForm'];
    protected $casts = ['status' => 'boolean'];

    public function setStatusAttribute($value){
		if($value == 'on'){
			$this->attributes['status'] = 1;
		}
	}

	public function setPasswordAttribute($value) {
    	$this->attributes['password'] = Hash::make($value);
	}

	public function scopeShow($query){
		return $query->orderBy('created_at', 'desc')->paginate(10);
	}

	public function setLogoAttribute($value){
		$fileName = fileHandler::customerImageStore($value);
		$this->attributes['logo'] = $fileName;
	}

	public function society(){
		return $this->belongsTo(society::class);
	}

	public function services(){
		return $this->hasMany(services::class);
	}

	public function customer_services(){
		return $this->hasMany(customer_services::class);
	}

	//this is accessor
	public function getStatusAttribute($attribute){
		return [
		null => 'Inactive',
		0 => 'Inactive',
		1=>'Active',
		][$attribute];
	}

	public function getGenderAttribute($attribute){
		return [
		1 => 'Male',
		2 => 'Female',
		][$attribute];
	}
}
