<?php

namespace App\Models;
use fileHandler;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\subcatagory;
use App\Models\services;
use App\Models\customer_services;

class category extends Model
{
    use HasFactory;
    public $table = "categories";
    protected $guarded=['submitForm'];
    protected $casts = ['status' => 'boolean'];

    public function setStatusAttribute($value){
		if($value == 'on'){
			$this->attributes['status'] = 1;
		}
	}

	//this is model scoping
	public function scopeShow($query){
		return $query->orderBy('created_at', 'desc')->paginate(10);
	}

	public function setLogoAttribute($value){
		$fileName = fileHandler::categoryImageStore($value);
		$this->attributes['logo'] = $fileName;
	}

	public function subcategories(){
		return $this->hasMany(subcatagory::class);
	} 

	public function services(){
		return $this->hasMany(services::class);
	}

	public function customer_services(){
		return $this->belongsTo(customer_services::class);
	}

	//this is accessor
	public function getStatusAttribute($attribute){
		return [
		null => 'Inactive',
		0 => 'Inactive',
		1=>'Active',
		][$attribute];
	}
}
