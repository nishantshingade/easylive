<?php

namespace App\Models;
use storage;
use employee;
use fileHandler;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    use HasFactory;
    public $table = "companies";
    protected $guarded=['submitForm'];
    
    //this is mutator
	public function setStatusAttribute($value){
		$this->attributes['status'] =  isset($value) ? 1 : 0;
	}

	//this is model scoping
	public function scopeShow($query){
		return $query->orderBy('created_at', 'desc')->paginate(10);
	}

	public function setLogoAttribute($value){
		$fileName = fileHandler::companyLogoStore($value);
		$this->attributes['logo'] = $fileName;
	}

	//this is accessor
	public function getStatusAttribute($attribute){
		return [
		2 => 'Inactive',
		1=>'Active',
		3 => 'Draft',
		][$attribute];
	}

	public function employees(){
		return $this->hasMany(employee::class);
	} 
}
