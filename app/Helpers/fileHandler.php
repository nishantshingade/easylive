<?php
namespace App\Helpers; // Your helpers namespace 
use App\User;
use App\Company;
use Auth;

class fileHandler{

	public static function companyLogoStore($value){		
      if(isset($value)){
        $extension = $value->getClientOriginalExtension();
        $fileName = time().'.'.$extension;
        $path = storage_path().'/app/public/images/companyLogos/';
        $uplaod = $value->move($path,$fileName);
      }else{
        $fileName ='';
      }
        return $fileName;
	}

  public static function societyImageStore($value){    
      if(isset($value)){
        $extension = $value->getClientOriginalExtension();
        $fileName = time().'.'.$extension;
        $path = storage_path().'/app/public/images/society/';
        $uplaod = $value->move($path,$fileName);
      }else{
        $fileName ='';
      }
        return $fileName;
  }

  public static function categoryImageStore($value){    
      if(isset($value)){
        $extension = $value->getClientOriginalExtension();
        $fileName = time().'.'.$extension;
        $path = storage_path().'/app/public/images/category/';
        $uplaod = $value->move($path,$fileName);
      }else{
        $fileName ='';
      }
        return $fileName;
  }

  public static function subcategoryImageStore($value){    
      if(isset($value)){
        $extension = $value->getClientOriginalExtension();
        $fileName = time().'.'.$extension;
        $path = storage_path().'/app/public/images/subcategory/';
        $uplaod = $value->move($path,$fileName);
      }else{
        $fileName ='';
      }
        return $fileName;
  }

  public static function serviceImageStore($value){    
      if(isset($value)){
        $extension = $value->getClientOriginalExtension();
        $fileName = time().'.'.$extension;
        $path = storage_path().'/app/public/images/services/';
        $uplaod = $value->move($path,$fileName);
      }else{
        $fileName ='';
      }
        return $fileName;
  }

  public static function customerImageStore($value){    
      if(isset($value)){
        $extension = $value->getClientOriginalExtension();
        $fileName = time().'.'.$extension;
        $path = storage_path().'/app/public/images/customers/';
        $uplaod = $value->move($path,$fileName);
      }else{
        $fileName ='';
      }
        return $fileName;
  }

}

?>