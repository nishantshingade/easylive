<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class customerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname'=>'bail|required|regex:/^[\s\w-]*$/|min:3|max:100',
            'lname'=>'bail|required|regex:/^[\s\w-]*$/|min:3|max:100',
            'mobile' => 'bail|required|numeric|digits:10',
            'email' => 'nullable|email',
            'gender' => 'required|numeric',
            'logo' => 'nullable',
            'society_id' => 'required|numeric',
            'wing' => 'nullable',
            'flat' => 'required|numeric',
            'floor' => 'required|numeric',
            'password' => 'required|max:250|confirmed',
            'password_confirmation' => 'required'

        ];
    }
}
