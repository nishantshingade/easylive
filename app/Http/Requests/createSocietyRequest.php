<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class createSocietyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'bail|required|regex:/^[\s\w-]*$/|min:3|max:50',
            'address' => 'nullable|regex:/(^[-0-9A-Za-z.,\/ ]+$)/|max:200',
            'chairman_name' => 'bail|required|regex:/^[\s\w-]*$/|min:3|max:50',
            'contact' => 'bail|required|numeric|digits:10',
            'establishment' => 'bail|nullable|date',
            'logo' => 'required'
        ];
    }
}
