<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class servicesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'bail|required|regex:/^[\s\w-]*$/|min:3|max:100',
            'desc' => 'nullable|regex:/^[\s\w-]*$/|max:200',
            'logo' => 'required',
            'category_id' => 'required',
            'subcatagories_id' => 'required',
            'rate' => "nullable|regex:/^\d+(\.\d{1,2})?$/",
            'commission' => "nullable|regex:/^\d+(\.\d{1,2})?$/"
        ];
    }
}
