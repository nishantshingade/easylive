<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class createCompanyFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        // return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'bail|required|regex:/^[\s\w-]*$/|min:3|max:50',
            'email' => 'bail|required|email|unique:companies|max:50',
            'address' => 'nullable|regex:/^[\s\w-]*$/|max:100',
            'contact' => 'bail|required|numeric',
            'establishment' => 'bail|nullable|date',
            'logo' => 'required'
        ];
    }
}
