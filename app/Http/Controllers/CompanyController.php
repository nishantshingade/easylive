<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use fileHandler;
use App\Models\company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\createCompanyFormRequest;

class CompanyController extends Controller
{
    public function index()
    {
        $list = company::show();
        return view('company/list',compact('list'));
    }
    
    public function create()
    {
    }

    public function store(createCompanyFormRequest $request)
    {
        Company::create($request->All());
        Alert::success('Success', 'Company Created!');
        return redirect('/company/list');
    }
    public function show(company $company)
    {
       
    }

    public function edit(company $company)
    {
        
    }

   
    public function update(Request $request, company $company)
    {
        //
    }

   
    public function destroy(company $company)
    {
        //
    }

    public function api(Request $request)
    {
        $pageSize = $request->size;
        $pageNumber = $request->page;
        $escapeItems = ($pageNumber*$pageSize)-$pageSize;
        $baseUrl = 'https://api.spacexdata.com/v3/'.$request->item.'?limit='.$request->size.'&offset='.$escapeItems;
        $response = Http::get($baseUrl);
        $data = json_decode($response->getBody());
        echo json_encode($data,JSON_PRETTY_PRINT);
    }

    public function q3(Request $request)
    {
        $input = explode(",",$request->key);
        $result = $data = $outputArray =array();
        $counter = 0;
        foreach($input as $key => $value){
            foreach($input as $key1 => $value1){
                if($key !== $key1){
                    if($value == $value1){
                        $data = ($key > $key1) ? $key.$key1 : $key1.$key;
                       $result[$counter] =  $data;
                       $counter++;
                    }
                }
            } 
        }  
        foreach($result as $inputArrayItem) {
            foreach($outputArray as $outputArrayItem) {
                if($inputArrayItem == $outputArrayItem) {
                    continue 2;
                }
            }
            $outputArray[] = $inputArrayItem;
        }
       return $outputArray;
    }

}
