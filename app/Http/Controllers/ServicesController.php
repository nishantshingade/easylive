<?php

namespace App\Http\Controllers;

use App\Models\services;
use App\Models\category;
use App\Models\subcatagory;
use Illuminate\Http\Request;
use App\Http\Requests\servicesRequest;
use RealRashid\SweetAlert\Facades\Alert;

class ServicesController extends Controller
{
    public function index()
    {
        $list = services::with(['category','subcatagory'])->paginate(10);
        return view('services/list',compact('list'));
    }

    public function create()
    {
        $categories = category::all('id','name');
        $subcategories = subcatagory::all('id','name');
        return view('services.new',compact('categories','subcategories'));
    }

    public function store(servicesRequest $request)
    {
        services::create($request->All());
        Alert::success('Success', 'Service Created!');
        return redirect('/services/list');
    }

    public function show(services $services)
    {
        //
    }

    public function edit(services $services)
    {
        //
    }

    public function update(Request $request, services $services)
    {
        //
    }

    public function destroy(services $services)
    {
        //
    }
}
