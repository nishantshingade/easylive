<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use App\Models\society;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\createSocietyRequest;



class societyController extends Controller
{
    public function index()
    {
        $list = society::show();
        return view('society/list',compact('list'));
    }
    public function store(createSocietyRequest $request)
    {
        society::create($request->All());
        Alert::success('Success', 'Society Created!');
        return redirect('/society/list');
    }
}
