<?php

namespace App\Http\Controllers;
use App\Models\category;
use App\Models\subcatagory;
use App\Models\services;
use Illuminate\Http\Request;
use App\Http\Requests\subcategoryRequest;
use RealRashid\SweetAlert\Facades\Alert;

class SubcatagoryController extends Controller
{
   
    public function index()
    {
        $list = subcatagory::show();
        return view('subcategory/list',compact('list'));
    }

    public function create()
    {
        $categories = category::all('id','name');
        return view('subcategory.new',compact('categories'));
    }

    public function store(subcategoryRequest $request)
    {
        subcatagory::create($request->All());
        Alert::success('Success', 'Sub Category Created!');
        return redirect('/subcategory/list');
    }

    public function show(subcatagory $subcategory)
    {
        $services = services::where('subcatagories_id','=',$subcategory->id)->get();
        return view('mobile.app.subcategory.show',compact('services'));
    }

    public function edit(subcatagory $subcatagory)
    {
        //
    }

    public function update(Request $request, subcatagory $subcatagory)
    {
        //
    }

    public function destroy(subcatagory $subcatagory)
    {
        //
    }
}
