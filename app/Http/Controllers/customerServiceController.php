<?php

namespace App\Http\Controllers;
use App\Models\customer_services;
use App\Models\services;
use Carbon\Carbon;
use Illuminate\Http\Request;

class customerServiceController extends Controller
{

	public function index()
    {
        $list = customer_services::with('customers','services','category','subcatagory')->orderBy('id', 'DESC')->paginate(15);
        return view('customers_services.list',compact('list'));
    }

    public function start(Request $request){
    	$data = array();
       if(isset($request->data['customer_id'])){
       	$data['customers_id'] = $request->data['customer_id'];
       }
       if(isset($request->data['from'])){
       	$data['start_date'] = Carbon::parse($request->data['from']);
       }
       if(isset($request->data['service_id'])){
       	$data['services_id'] = $request->data['service_id'];
       }
       if(isset($request->data['to'])){
       	$data['end_date'] = Carbon::parse($request->data['to']);
       }
       $serviceDetails = services::whereId($data['services_id'])->first();
       $data['category_id'] = $serviceDetails->category_id;
       $data['subcatagories_id'] = $serviceDetails->subcatagories_id;
       $data['rate'] = $serviceDetails->rate;
       $data['commission'] = $serviceDetails->commission;
       $data['total'] = $data['rate']+$data['commission'];
       $data['no_of_days'] = $data['end_date']->diff($data['start_date'])->days;
       $data['status'] = 1;
       if($data['subcatagories_id'] == 6){
        $data['no_of_days'] =$data['no_of_days']+1;
       	$data['milk_coupens'] = $data['no_of_days'];
        $data['total'] =$data['total']*$data['no_of_days'];
       }else{
       	$data['milk_coupens'] = 0;
       }
        $result = customer_services::create($data);
        return $result;
    }

    public function stop(){
      
    }
}
