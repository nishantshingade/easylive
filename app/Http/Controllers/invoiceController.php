<?php

namespace App\Http\Controllers;
use DB;
use Carbon\Carbon;
use App\Models\society;
use App\Models\customers;
use App\Models\customer_services;
use Illuminate\Http\Request;
use App\Http\Requests\invoiceCustomerRequest; 
use App\Http\Controllers\numberToWordConverterController;

class invoiceController extends Controller
{
    
    public function index()
    {
        $list = array();
        $societies = society::active();
        return view('invoice.view',compact('societies','list'));
    }

    
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(invoiceCustomerRequest $request)
    {
        $month = date("m",strtotime($request->month));
        $societies = society::active();
        if($request->mobile){

            $list = DB::table('customer_services')
            ->join('customers', 'customers.id', '=', 'customer_services.customers_id')
            ->join('society', 'society.id', '=', 'customers.society_id')
            ->join('services', 'services.id', '=', 'customer_services.services_id')
            ->select('services.id','customer_services.customers_id','customers.fname','customers.lname','customers.mobile','customers.wing','customers.floor','customers.flat',DB::raw('GROUP_CONCAT(services.name SEPARATOR ", ") as serviceList'),'customer_services.start_date','customer_services.end_date','customer_services.no_of_days','customer_services.status')
            ->where('society.id', '=', $request->society)
            ->where('customers.mobile', '=', $request->mobile)
            ->whereMonth('customer_services.created_at', $month)
            ->groupBy('customer_services.customers_id')
            ->paginate(10);
        }else if($request->wing){
            $list = DB::table('customer_services')
            ->join('customers', 'customers.id', '=', 'customer_services.customers_id')
            ->join('society', 'society.id', '=', 'customers.society_id')
            ->join('services', 'services.id', '=', 'customer_services.services_id')
            ->select('services.id','customer_services.customers_id','customers.fname','customers.lname','customers.mobile','customers.wing','customers.floor','customers.flat',DB::raw('GROUP_CONCAT(services.name SEPARATOR ", ") as serviceList'),'customer_services.start_date','customer_services.end_date','customer_services.no_of_days','customer_services.status')
            ->where('society.id', '=', $request->society)
            ->where('customers.wing', '=', $request->wing)
            ->whereMonth('customer_services.created_at', $month)
            ->groupBy('customer_services.customers_id')
            ->paginate(10);
        }else if($request->wing && $request->flat){
            $list = DB::table('customer_services')
            ->join('customers', 'customers.id', '=', 'customer_services.customers_id')
            ->join('society', 'society.id', '=', 'customers.society_id')
            ->join('services', 'services.id', '=', 'customer_services.services_id')
            ->select('services.id','customer_services.customers_id','customers.fname','customers.lname','customers.mobile','customers.wing','customers.floor','customers.flat',DB::raw('GROUP_CONCAT(services.name SEPARATOR ", ") as serviceList'),'customer_services.start_date','customer_services.end_date','customer_services.no_of_days','customer_services.status')
            ->where('society.id', '=', $request->society)
            ->where('customers.wing', '=', $request->wing)
            ->where('customers.flat', '=', $request->flat)
            ->whereMonth('customer_services.created_at', $month)
            ->groupBy('customer_services.customers_id')
            ->paginate(10);
        }else{
            $list = DB::table('customer_services')
            ->join('customers', 'customers.id', '=', 'customer_services.customers_id')
            ->join('society', 'society.id', '=', 'customers.society_id')
            ->join('services', 'services.id', '=', 'customer_services.services_id')
            ->select('services.id','customer_services.customers_id','customers.fname','customers.lname','customers.mobile','customers.wing','customers.floor','customers.flat',DB::raw('GROUP_CONCAT(services.name SEPARATOR ", ") as serviceList'),'customer_services.start_date','customer_services.end_date','customer_services.no_of_days','customer_services.status')
            ->where('society.id', '=', $request->society)
            ->whereMonth('customer_services.created_at', $month)
            ->groupBy('customer_services.customers_id')
            ->paginate(10);
        }
        
        
        return view('invoice.view',compact('list','societies','month'));
    }

    public function print(customers $customer,Request $request)
    {
        $from = array('name'=>'Live Easy','address'=>'Auralis Tower, Hajuri Gaon, Opp. Lic Office, Thane (west) 400604 ','email'=>'liveeasy2021@gmail.com','mobile'=>'8097090145 / 8097090146');
        $month = $request->month;
        $total = 0;
        $list = DB::table('customer_services')
            ->join('customers', 'customers.id', '=', 'customer_services.customers_id')
            ->join('society', 'society.id', '=', 'customers.society_id')
            ->join('services', 'services.id', '=', 'customer_services.services_id')
            ->select('services.id as serviceId','society.name as societyName','society.address as societyAddr','services.name as serviceName','customer_services.start_date','customer_services.end_date','customer_services.no_of_days','customer_services.rate','customer_services.commission','customer_services.total')
            ->where('customer_services.customers_id', '=', $customer->id)->whereMonth('customer_services.created_at',$month)->get();
        foreach($list as $eachservice){
            $total+=$eachservice->total;
        }
        $digit = new numberToWordConverterController;
        $inWords =  $digit->convert_number_to_words((string)round($total,0));
        return view('invoice.print',compact('list','total','month','from','customer','inWords'));
    }

}
