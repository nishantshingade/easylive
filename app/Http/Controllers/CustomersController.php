<?php

namespace App\Http\Controllers;
use Session;
use App\Models\customers;
use App\Models\society;
use Illuminate\Http\Request;
use App\Http\Requests\customerRequest;
use RealRashid\SweetAlert\Facades\Alert;

class CustomersController extends Controller
{

    public function index()
    {
        $list = customers::with('society')->paginate(10);
        return view('customers/list',compact('list'));
    }

    public function create()
    {
       $societies = society::all('id','name');
       return view('customers.new',compact('societies'));
    }

    public function store(customerRequest $request)
    {
        if($request->ismobile == 0){
            customers::create($request->All());
            Alert::success('Success', 'Customer Created!');
            return redirect('/customers/list');
        }else{
            $id = customers::create($request->All())->id;
            Alert::success('Success', 'Customer Created!');
            Session::put('user', [ 'id' => $id, 'name' => $request->fname.' '.$request->lname ]);
            return redirect('/mobile/dashboard');
        }
        
    }

    public function show(customers $customers)
    {
        //
    }

    public function edit(customers $customers)
    {
        //
    }

    public function update(Request $request, customers $customers)
    {
        //
    }

    public function destroy(customers $customers)
    {
        //
    }
}
