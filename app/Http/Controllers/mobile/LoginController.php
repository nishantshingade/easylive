<?php

namespace App\Http\Controllers\mobile;
use Hash;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\mobileLoginRequest;
use App\Models\customers;
use App\Models\category;
use App\Models\society;
use App\Models\customer_services;
use RealRashid\SweetAlert\Facades\Alert;


class LoginController extends Controller
{
    public function index(){
        return view('mobile.login.index');
    }

    public function logout(){
      Session::forget('user');
      return redirect('/mobile/login');
    }

    public function register(){
      $societies = society::all('id','name');
      return view('mobile.register.index',compact('societies'));
    }

    public function loginCheck(mobileLoginRequest $request){
       
       $user = customers::with('society','customer_services')->where('mobile', '=', $request->mobile)->first();
       if($user != null){
          $myServices = customer_services::with('customers','services','category','subcatagory')->where('customers_id','=',$user->id)->where('status','=',1)->get();
          $status = Hash::check($request->password, $user->password);
          if($status){
            Session::put('user', [ 'id' => $user->id, 'name' => $user->fname.' '.$user->lname ]);
            return redirect('/mobile/dashboard');
          }else{ 
            Alert::error('Error', 'Invalid Credientials Provided');
            return redirect('/mobile/login');
        }
       }else{
            Alert::error('Error', 'Invalid Credientials Provided');
            return redirect('/mobile/login');
       } 
    }

    public function dashboard(){
      $user= Session::get('user');
      if($user !== null || $user !== ''){
        $myServices = customer_services::with('customers','services','category','subcatagory')->where('customers_id','=',$user['id'])->where('status','=',1)->get();
        $categories = category::all('id','name','logo');
        return view('mobile.app.dashboard',compact('categories','myServices'));
      }else{
        Alert::error('Error', 'Invalid Credientials Provided');
        return redirect('/mobile/login');
      }
      
    }
}
