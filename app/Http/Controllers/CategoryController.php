<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\subcatagory;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\categoryRequest;
use Illuminate\Validation\ValidationException;

class CategoryController extends Controller
{
    
    public function index()
    {
        $list = category::show();
        return view('category/list',compact('list'));
    }

    public function create()
    {
        //
    }

    public function store(categoryRequest $request)
    {
        category::create($request->All());
        Alert::success('Success', 'Category Created!');
        return redirect('/category/list');
    }

    
    public function show(category $category)
    {
        $data = category::where('id','=',$category->id)->with('subcategories')->get();
        return view('mobile.app.category.show',compact('data'));
    }

   
    public function edit(category $category)
    {
        //
    }

   
    public function update(Request $request, category $category)
    {
        //
    }

    public function destroy(category $category)
    {
        //
    }
}
