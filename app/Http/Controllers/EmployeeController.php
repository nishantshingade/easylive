<?php

namespace App\Http\Controllers;
use App\Models\company;
use App\Models\employee;
use Illuminate\Http\Request;
use App\Http\Requests\employeeRequest;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Validation\ValidationException;


class EmployeeController extends Controller
{
    
    public function index()
    {
        $list = employee::show();
        return view('employee.list',compact('list'));
    }

    public function create()
    {
        $companies = company::all('id','name');
        return view('employee.new',compact('companies'));
    }
    public function store(employeeRequest $request)
    {
        employee::create($request->validated());
        Alert::success('Employee Created', 'Employee Added to the System !');
        return redirect('employee/list');
    }
    public function show(employee $employee)
    {
        //
    }

    public function edit(employee $employee)
    {
        //
    }

    public function update(Request $request, employee $employee)
    {
        //
    }

    public function destroy(employee $employee)
    {
        //
    }
}
