<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\societyController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubcatagoryController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\mobile\LoginController;
use App\Http\Controllers\mobile\invoiceController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {	
    return view('welcome');
});
Route::get('/home', function () {
    return view('dashboard');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/q1', function () {
    return view('question.one');
})->name('question.one');
Route::middleware(['auth:sanctum', 'verified'])->get('/q2', function () {
    return view('question.two');
})->name('question.two');
Route::middleware(['auth:sanctum', 'verified'])->get('/q3', function () {
    return view('question.three');
})->name('question.three');
Route::post('/q3/ans', 'App\Http\Controllers\CompanyController@q3')->name('q3.ans');
Route::middleware(['auth:sanctum', 'verified'])->get('/q4', function () {
    return view('question.four');
})->name('question.four');
Route::middleware(['auth:sanctum', 'verified'])->get('/q6', function () {
    return view('question.six');
})->name('question.six');
Route::middleware(['auth:sanctum', 'verified'])->get('/aws', function () {
    return view('question.aws');
})->name('question.aws');



Route::middleware(['auth:sanctum', 'verified'])->get('/company/new', function () {
    return view('company.new');
})->name('company.new');
Route::post('/company/store', 'App\Http\Controllers\CompanyController@store')->name('storeCompany');
Route::get('/company/list', 'App\Http\Controllers\CompanyController@index')->name('listCompany');
Route::get('/employee/new', 'App\Http\Controllers\EmployeeController@create')->name('createEmployee');
Route::post('/employee/store', 'App\Http\Controllers\EmployeeController@store')->name('storeEmployee');
Route::get('/employee/list', 'App\Http\Controllers\EmployeeController@index')->name('listEmployee');



Route::middleware(['auth:sanctum', 'verified'])->get('/society/new', function () {
    return view('society.new');
})->name('society.new');
Route::post('/society/store', 'App\Http\Controllers\societyController@store')->name('storeSociety');
Route::get('/society/list', 'App\Http\Controllers\societyController@index')->name('listSociety');
Route::get('/society/edit/{society}', 'App\Http\Controllers\societyController@edit')->name('editSociety');


Route::middleware(['auth:sanctum', 'verified'])->get('/category/new', function () {
    return view('category.new');
})->name('category.new');
Route::post('/category/store', 'App\Http\Controllers\CategoryController@store')->name('storeCategory');
Route::get('/category/list', 'App\Http\Controllers\CategoryController@index')->name('listCategory');

Route::middleware(['auth:sanctum', 'verified'])->get('/subcategory/new', 'App\Http\Controllers\SubcatagoryController@create')->name('subcategory.new');
Route::post('/subcategory/store', 'App\Http\Controllers\SubcatagoryController@store')->name('storesubCategory');
Route::get('/subcategory/list', 'App\Http\Controllers\SubcatagoryController@index')->name('listsubCategory');

Route::middleware(['auth:sanctum', 'verified'])->get('/services/new', 'App\Http\Controllers\ServicesController@create')->name('services.new');
Route::post('/services/store', 'App\Http\Controllers\ServicesController@store')->name('storeServices');
Route::get('/services/list', 'App\Http\Controllers\ServicesController@index')->name('listServices');

Route::middleware(['auth:sanctum', 'verified'])->get('/customers/new', 'App\Http\Controllers\CustomersController@create')->name('customers.new');
Route::post('/customers/store', 'App\Http\Controllers\CustomersController@store')->name('storeCustomers');
Route::get('/customers/list', 'App\Http\Controllers\CustomersController@index')->name('listCustomers');


Route::get('/mobile/login', 'App\Http\Controllers\mobile\LoginController@index')->name('mobileLogin');
Route::get('/mobile/register', 'App\Http\Controllers\mobile\LoginController@register')->name('mobileRegister');
Route::post('/mobile/loginCheck', 'App\Http\Controllers\mobile\LoginController@loginCheck')->name('loginCheck');
Route::get('/mobile/dashboard', 'App\Http\Controllers\mobile\LoginController@dashboard')->name('dashboard');
Route::get('/mobile/logout', 'App\Http\Controllers\mobile\LoginController@logout')->name('logout');



Route::get('/mobile/category/{category}', 'App\Http\Controllers\CategoryController@show')->name('mobileCategoryShow');
Route::get('/mobile/subcategory/{subcategory}', 'App\Http\Controllers\SubcatagoryController@show')->name('mobileSubCategoryShow');

Route::post('/service/start', 'App\Http\Controllers\customerServiceController@start')->name('mobileServiceStart');
Route::get('/customers/service/list', 'App\Http\Controllers\customerServiceController@index')->name('listCustomerServices');
Route::get('/customers/service/new', 'App\Http\Controllers\customerServiceController@create')->name('createCustomerServices');
Route::post('/customers/service/stop', 'App\Http\Controllers\customerServiceController@stop')->name('stopCustomerServices');

Route::middleware(['auth:sanctum', 'verified'])->get('/customers/invoice', 'App\Http\Controllers\invoiceController@index')->name('invoice');
Route::middleware(['auth:sanctum', 'verified'])->post('/customers/invoice', 'App\Http\Controllers\invoiceController@show')->name('invoiceShow');
Route::middleware(['auth:sanctum', 'verified'])->get('/customers/invoice/print/{customer}/{month}', 'App\Http\Controllers\invoiceController@print')->name('invoicePrint');
