<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('society_id');
            $table->string('fname');
            $table->string('lname');
            $table->string('password');
            $table->string('mobile')->unique();
            $table->integer('email')->unique()->nullable();
            $table->integer('gender');
            $table->string('logo')->nullable();
            $table->string('wing')->nullable();
            $table->string('floor');
            $table->string('flat');
            $table->integer('status');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
